FROM python:3.7-alpine

WORKDIR /app

RUN apk add --no-cache --virtual .build-deps \
    gcc \
    python3-dev \
    musl-dev

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . ./

CMD ["python", "-m", "pytest", "tests", "--mypy"]
