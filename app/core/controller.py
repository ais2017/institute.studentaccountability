# -*- coding: utf-8 -*-
import datetime

from .academic_list import (
    MaterialIncentivesList,
    DeductionList,
    AcademicControlList,
    AcademicPerformanceList)

from .extern_driver import (
    ExternSystemConsumer,
    ExternSystemJSONConsumer)


class StudentAccountAbilityController:
    """Dummy integration layer for buisness logic components."""

    # Used to consume data from external system API.
    extern_driver = None  # type: ExternSystemConsumer

    def __init__(self, extern_interface: str, **kwargs):
        """
        :param extern_interface: Way to access data from external system.
               Without real connection I think only about json fixtures here.
               Allowed values: ["json"]
        :param kwargs: For JSON interface should has list of locations.
        """
        if extern_interface == "json":
            if not kwargs.get("locations", None):
                raise AttributeError("JSON interface required locations list")
            self.extern_driver = ExternSystemJSONConsumer(kwargs["locations"])
        else:
            raise ValueError("Unknown ExternSystem interface type")

    @property
    def extern_interface(self) -> str:
        return str(self.extern_driver)

    def get_material_incentives_list(self) -> str:
        """Generate MaterialIncentivesList representation for all students."""
        students = self.extern_driver.fetch_students()  # type: List[Student]
        budget_limit = self.extern_driver.fetch_budget_limit()  # type: int
        l = MaterialIncentivesList("Material incentivies",
                students, 0, budget_limit)
        return l.get_report()

    def get_deduction_list(self) -> str:
        """Generate DeductionList representation for all students."""
        students = self.extern_driver.fetch_students()  # type: List[Student]
        debt = self.extern_driver.fetch_debt_coefficient()  # type: float
        l = DeductionList("Deduction incentivies",
                students, debt)
        return l.get_report()

    def get_academic_control_list(self) -> str:
        """Generate AcademicControlList representation for this semester."""
        students = self.extern_driver.fetch_students()  # type: List[Student]
        debt = self.extern_driver.fetch_debt_coefficient()  # type: float
        l = AcademicControlList(
                "Academic control for current semester",
                students,
                datetime.datetime.today() - datetime.timedelta(days=180),
                datetime.datetime.today())
        return l.get_report()

    def get_academic_performance_list(self) -> str:
        """Generate AcademicPerformanceList representation."""
        students = self.extern_driver.fetch_students()  # type: List[Student]
        l = AcademicPerformanceList(
                "Overall academic performace",
                students)
        return l.get_report()
