# -*- coding: utf-8 -*-
from __future__ import absolute_import

import abc
import json
import os
from typing import List, Tuple, Dict

from ..models.institute import (
    Student,
    Group,
    Subject,
    Lecturer)


class ExternSystemConsumer:
    __metaclass__ = abc.ABCMeta

    _interface = ""  # type: str

    @abc.abstractmethod
    def fetch_students(self) -> List[Student]:
        """Fetch list of all available students from external system."""

    @abc.abstractmethod
    def fetch_study_data(self) -> Tuple[List[Group], List[Lecturer]]:
        """Fetch complete curriculum for all student groups and lecturers
        from external system."""

    @abc.abstractmethod
    def fetch_budget_limit(self) -> int:
        """Fetch budget limit from external system."""

    @abc.abstractmethod
    def fetch_debt_coefficient(self) -> float:
        """Fetch debt coefficient from external system."""


class ExternSystemJSONConsumer(ExternSystemConsumer):
    """Manages data consumption from external university system represented
    in JSON format.

    There are two required json fixtures:
    * students.json: Students database
    * metadata.json: General information about institute
    """

    _interface = "json"
    _required_files = ["metadata", "students"]
    _locations = {}  # type: Dict[str, str]

    def __init__(self, locations: List[str]):
        """
        :param locations: JSON data files locations.
        """
        for fp in locations:
            if not os.path.isfile(fp):
                raise OSError("Location unavailable")
            if "students.json" in fp:
                self._locations["students"] = fp
            if "metadata.json" in fp:
                self._locations["metadata"] = fp

        if len(self._locations) != len(self._required_files):
            raise AttributeError(f"Missing locations!")

    def __str__(self):
        return self._interface

    @property
    def locations(self) -> List[str]:
        return self._locations

    def fetch_students(self) -> List[Student]:
        students = []  # type: List[Student]
        with open(self._locations["students"]) as f:
            json_data = json.load(f)
            groups = {}
            for gd in json_data.get("groups"):
                g_subjects = [Subject(s) for s in gd["subjects"]]
                groups.update(
                    {gd["name"]: Group(gd["name"], gd["faculty"], g_subjects)})
            for sd in json_data.get("students"):
                additional_subjects = [Subject(s)
                                       for s in sd["additional_subjects"]]
                students.append(
                    Student(sd["name"], groups[sd["group"]], additional_subjects))
        return students

    def fetch_study_data(self) -> Tuple[List[Group], List[Lecturer]]:
        groups = []
        lecturers = []

        with open(self._locations["metadata"]) as f:
            json_data = json.load(f)
            for gd in json_data.get("groups"):
                g_subjects = [Subject(s) for s in gd["subjects"]]
                groups.append(Group(gd["name"], gd["faculty"], g_subjects))
            for ld in json_data.get("lecturers"):
                # mypy: begin ignore
                l_subjects = [Subject(s) for s in ld["subjects"]]
                lecturers.append(Lecturer(ld["name"], l_subjects))
                # mypy: end ignore
        return groups, lecturers

    def fetch_budget_limit(self) -> int:
        with open(self._locations["metadata"]) as f:
            json_data = json.load(f)
            limit = json_data.get("budget_limit", 0)
            try:
                return int(limit)
            except ValueError:
                raise ValueError("`metadata.json` is broken")

    def fetch_debt_coefficient(self) -> float:
        with open(self._locations["metadata"]) as f:
            json_data = json.load(f)
            debt = json_data.get("debt_coefficient", 0)
            try:
                return float(debt)
            except ValueError:
                raise ValueError("`metadata.json` is broken")

