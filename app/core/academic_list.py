# -*- coding: utf-8 -*-
import abc
import json

from datetime import date
from typing import List

from ..models.institute import Student


class ReportFormatter:
    """Represents a report in human-readable format."""

    # Supported file types to generate reports
    REPORT_TYPES = ["csv"]

    def __init__(self, fmt: str):
        """
        :param fmt: One of supported report formats
        """
        if fmt not in self.REPORT_TYPES:
            raise ValueError("Unknown report type!")
        self._fmt = fmt

    @property
    def fmt(self) -> str:
        return self._fmt

    @fmt.setter
    def fmt(self, value: str):
        if value not in self.REPORT_TYPES:
            raise ValueError("Unknown report type!")
        self._fmt = value

    def get_line(self, s: Student):
        """Get single line of report selected format.

        For now I suppose that we supports only csv format, so I use it here.
        Later it can be extended with other formats impelemented as
        sub-classes.
        """
        return f"{s.name};{s.group.name}"

    def get_title(self, values: List[str]):
        """Generate title string for current format"""
        return ';'.join(values)


class AcademicList:
    """Genertic academic list of institute students."""

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_report(self, fmt: str):
        """Generate report in selected format from `REPORT_TYPES`."""
        pass

    def __init__(self, name: str, students: List[Student]):
        """
        :param name: Name of the list
        :param students: List of engaged students
        """
        self._name = name
        self._students = students
        self._report_formatter = ReportFormatter("csv")

    def __len__(self):
        return len(self._students)

    def __repr__(self):
        for i, s in enumerate(self._students):
            print(f"{i}: {s}")

    def append(self, student: Student):
        self._students.append(student)

    def __getitem__(self, num: int) -> Student:
        return self._students[num]

    def sort(self):
        self._students.sort(key=lambda s: sum(
            [sum(ms) for ms in s.marks.values()]))

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        if len(value) > 0:
            self._name = value
        else:
            raise ValueError("Name can't be empty!")

    def to_json(self):
        """Serialize report in JSON to subsequent transmission."""
        return json.dumps({
            "name": self._name,
            "students": [s.to_json()for s in self._students],
        })


class MaterialIncentivesList(AcademicList):
    """Account list that contains cash bonuses for students."""

    def __init__(self, name, students, cash_min: int, cash_limit: int):
        """
        :param cash_min: Minimal cash addition defined by regulatory documents
        :param cash_limit: Cash limit from institute's tangible fund
        """
        if cash_limit < 0 or cash_min < 0:
            raise ValueError("Cash can't be negative!")

        self._cash_min = cash_min
        self._cash_limit = cash_limit
        super().__init__(name, students)

        self._evaluate_addition()

    def _evaluate_addition(self):
        """Evaluates additional cash for students."""
        avg_cash = self._cash_limit // len(self._students)
        for s in self._students:
            #  s.update({"additional_cache": avg_cash})
            pass

    def __repr__(self):
        for i, s in enumerate(self._students):
            print(f"{i}: {s} Additional cash: {s.get('additional_cache')}")

    @property
    def cash_limit(self):
        return self._cash_limit

    @cash_limit.setter
    def cash_limit(self, value):
        if value < 0:
            raise ValueError("Cash can't be negative!")
        self._cash_limit = value

    def to_json(self):
        return json.dumps({
            "name": self._name,
            "cash_limit": self._cash_limit,
            "students": [s.to_json() for s in self._students],
        })

    def get_report(self, fmt: str) -> str:
        report = self._report_formatter.get_title(
            [self.name, self.cash_limit])
        report += '\n'.join([self._report_formatter.get_line(s)
                             for s in self._students])
        return report


class DeductionList(AcademicList):
    """Account list with students proposed for deduction."""

    def __init__(self, name, students, debt_coefficient: float = 0.33):
        """
        :param debt_coefficient: Coefficient of negative marks.
        """
        if debt_coefficient <= 0 or debt_coefficient >= 1:
            raise ValueError("Incorrect debt coefficient")
        self._debt_coefficient = debt_coefficient
        super().__init__(name, students)

    def sort(self):
        self._students.sort(key=lambda s: sum(
            [sum(ms) for ms in s.marks.values()]))
        self._students.reverse()

    def get_report(self, fmt: str) -> str:
        report = self._report_formatter.get_title(
            [self.name])
        report += '\n'.join([self._report_formatter.get_line(s)
                             for s in self._students])
        return report

    @property
    def debt_coefficient(self) -> float:
        return self._debt_coefficient

    @debt_coefficient.setter
    def debt_coefficient(self, value: float):
        if not value:
            raise ValueError("Debt coefficient can't be unset")
        if value <= 0 or value >= 1:
            raise ValueError("Incorrect debt coefficient")
        self._debt_coefficient = value


class AcademicControlList(AcademicList):
    """Account list with half semester academic performance."""

    def __init__(self, name, students, date_start: date, date_end: date):
        """
        :param date_start: Start date of control period
        :param date_end: End date of control period
        """
        if not date_start or not date_end:
            raise ValueError("Date range can't be empty")
        if date_start > date_end:
            raise ValueError("Incorrect dates range")

        self._date_start = date_start
        self._date_end = date_end
        super().__init__(name, students)

    def to_json(self):
        return json.dumps({
            "name": self._name,
            "date_start": self._date_start.isoformat(),
            "date_end": self._date_start.isoformat(),
            "students": [s.to_json() for s in self._students],
        })

    @property
    def date_start(self) -> date:
        return self._date_start

    @date_start.setter
    def date_start(self, value: date):
        if not value:
            raise ValueError("Date can't be unset")
        if self._date_end < value:
            raise ValueError("Incorrect start date")
        self._date_start = value

    @property
    def date_end(self) -> date:
        return self._date_start

    @date_end.setter
    def date_end(self, value: date):
        if not value:
            raise ValueError("Date can't be unset")
        if value < self._date_start:
            raise ValueError("Incorrect end date")
        self._date_end = value

    def get_control_period_weeks(self) -> int:
        """Returns duration of control period in weeks."""
        return self._date_end.isocalendar()[1] - self._date_start.isocalendar()[1]

    def get_report(self, fmt: str) -> str:
        report = self._report_formatter.get_title(
            [self.name, self._date_start.isoformat(), self.date_end.isoformat()])
        report += '\n'.join([self._report_formatter.get_line(s)
                             for s in self._students])
        return report


class AcademicPerformanceList(AcademicList):
    """Account list with overall academic performance for group of students."""

    def __init__(self, name, students):
        super().__init__(name, students)

    def get_report(self, fmt: str) -> str:
        report = self._report_formatter.get_title([self.name])
        report += '\n'.join([self._report_formatter.get_line(s)
                             for s in self._students])
        return report
