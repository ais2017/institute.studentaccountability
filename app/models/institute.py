# -*- coding: utf-8 -*-
"""
Core models that describes Institute system buisness objects.
"""
import json

from typing import List, Dict


class Subject:
    """Academic subject.

    Later there maybe links to metadata from external system.
    """

    def __init__(self, name: str):
        """
        :param name: Name of subject.
        """
        self._name = name

    def __repr__(self):
        return f"Academic subject '{self._name}'"

    @property
    def name(self) -> str:
        return self._name


class Group:
    """Group of students."""

    def __init__(self, name, faculty: str,
                 subjects: List[Subject],
                 students=[]):
        """
        :param name: Name of group
        :param faculty: Name of faculty of this group
        :param subject: List of required academic subjects for each student
                        of that group.
        :param students: List of students from this group
        """
        self._name = name
        self._students = students
        self._faculty = faculty
        self._subjects = subjects

    def __repr__(self):
        return f"Group '{self._name}' (f. {self._faculty})"

    @property
    def subjects(self) -> List[Subject]:
        return self._subjects

    @subjects.setter
    def subjects(self, subjects: List[Subject]):
        self._subjects = subjects

    @property
    def students(self):
        return self._students

    @students.setter
    def students(self, students):
        self._students = students

    def add_student(self, student):
        if student in self._students:
            raise ValueError("Student already in this group")
        student.group = self
        self._students.append(student)

    @property
    def name(self) -> str:
        return self._name

    def add_subject(self, subject: Subject):
        if not subject:
            raise ValueError("Can't add empty subject")
        if subject in self._subjects:
            raise ValueError("Subject already exists")
        self._subjects.append(subject)

    @property
    def faculty(self) -> str:
        return self._faculty

    def to_json(self):
        return json.dumps({
            "name": self._name,
            "faculty": self._faculty,
        })


class Lecturer:

    def __init__(self, name: str, subjects: List[Subject]):
        """
        :param name: Name of lecturer
        :param subjects: Subjects that this teacher read.
        """
        self._name = name
        self._subjects = subjects

    def __repr__(self):
        return f"{self._name} (lecturer)"

    @property
    def subjects(self) -> List[Subject]:
        return self._subjects

    @subjects.setter
    def subjects(self, values: List[Subject]):
        self._subjects = values

    def add_subject(self, subject: Subject):
        if not subject:
            raise ValueError("Can't insert empty subject")
        if subject in self._subjects:
            raise ValueError("Subject already exists")
        self._subjects.append(subject)


class Student:
    """Represent student instance got from external service."""

    def __init__(self, name: str,
                 group: Group,
                 additional_subjects: List[Subject] = []):
        """
        :param name: Name of student
        :param group: Student's study group
        :param additional_subjects: List of additional study subjects for this
                                    student.
        """
        self._name = name
        self._group = group
        self._additional_subjects = additional_subjects

        all_subjects = self._group._subjects + additional_subjects
        self._marks: Dict[Subject, List[int]] = dict.fromkeys(all_subjects, [])

    @property
    def additional_subjects(self):
        return self._additional_subjects

    @additional_subjects.setter
    def additional_subjects(self, values: List[Subject]):
        self._additional_subjects = values

    def add_additional_subject(self, subject: Subject):
        if subject in self.subjects:
            raise ValueError("Subjects already exists")
        self._additional_subjects.append(subject)

    @property
    def subjects(self) -> List[Subject]:
        return list(self._marks.keys())

    @property
    def group(self) -> Group:
        return self._group

    @group.setter
    def group(self, group: Group):
        self._group = group

    @property
    def name(self) -> str:
        return self._name

    @property
    def marks(self) -> Dict[Subject, List[int]]:
        return self._marks

    @marks.setter
    def marks(self, values: Dict[Subject, List[int]]):
        """Set up mark for each of selected subjects."""
        for dv in values.values():
            if any([v not in range(0, 6) for v in dv]):
                raise ValueError("Incorrect input marks values")
        self._marks = values

    def get_marks(self, subject: Subject) -> List[int]:
        """Get marks for a single subject."""
        if subject not in self._group.subjects:
            raise ValueError("This student doesn't learn selected subject")
        return self._marks.get(subject, [])

    def insert_mark(self, subject: Subject, mark: int):
        """Add mark for single subject."""
        if mark not in range(0, 6):
            raise ValueError("Incorrect input mark value")

        try:
            self._marks[subject].append(mark)
        except KeyError:
            raise ValueError("This student doesn't learn selected subject")

    def __repr__(self):
        return f"{self._name} (student of {self._group})"

    def to_json(self):
        return json.dumps({
            "name": self._name,
            "group": self._group.to_json(),
            "marks": {subj.name: marks for subj, marks in self._marks.items()},
        })
