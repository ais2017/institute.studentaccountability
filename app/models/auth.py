# -*- coding: utf-8 -*-
"""
Represents user's of accountability system.

I apologize that it can be extended to flask app with
Flask-Login/Flask-SQLAlchemy based authentication.

In this case we have following tools:
    * flask_sqlalchemy.SQLAlchemy.Model to ORM implementation
    * flask_login.LoginManager to register required roles
    * flask_login.UserMixin with additional auth routines and DB fields

"""
import enum

from typing import Dict
from werkzeug.security import generate_password_hash

from .institute import Group, Subject, Lecturer


class AccessLevel(enum.Enum):
    """User access level for her resources."""
    NOACCESS = 0
    READ = 1
    WRITE = 2


class AccessDeniedError(Exception):
    """Should be replaced with LoginManager routines."""

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)


class Permissions:
    """User permissions for system's resources."""

    def __init__(self, name: str,
                 groups_access: Dict[Group, AccessLevel] = {},
                 subjects_access: Dict[Subject, AccessLevel] = {},
                 is_default=False):
        """
        :param groups_access: Access mode to each of user's group
        :param subject: Access mode to each of user's subjects
        """
        self._name = name
        self._groups_access: Dict[Group, AccessLevel] = groups_access
        self._subjects_access: Dict[Subject, AccessLevel] = subjects_access
        self.is_default = is_default

    def get_access(self, group: Group, subject: Subject) -> AccessLevel:
        """
        :return: User's access level to requested resources.
        """
        if (group in self._groups_access.keys()) and (
                subject in self._subjects_access.keys()):
            return min([
                self._groups_access.get(group, AccessLevel.NOACCESS),
                self._subjects_access.get(subject, AccessLevel.NOACCESS)])
        return AccessLevel.NOACCESS

    @property
    def groups(self):
        return self._groups_access.keys()

    def update_group(self, group: Group, access: AccessLevel):
        if group not in self._groups_access.keys():
            raise ValueError("Group not found")
        self._groups_access.update({group: access})

    @property
    def subjects(self):
        return self._subjects_access.keys()

    def update_subject(self, subject: Subject, access: AccessLevel):
        if subject not in self._subjects_access.keys():
            raise ValueError("Subject not found")
        self._subjects_access.update({subject: access})


class FacultyManagerPermissions(Permissions):

    def __init__(self, faculty: str, *args, **kwargs):
        """
        :param faculty: Name of faculty
        """
        self._faculty: str = faculty
        super().__init__(*args, **kwargs)

    @property
    def faculty(self) -> str:
        return self._faculty

    def get_access(self, group: Group, *args, **kwargs) -> AccessLevel:
        group_access = self._groups_access.get(group, None)
        if not group_access:
            return AccessLevel.NOACCESS
        if group.faculty != self._faculty:
            return AccessLevel.NOACCESS
        return group_access

    def update_group(self, group: Group, access: AccessLevel):
        if group not in self._groups_access.keys():
            raise ValueError("Group not found")
        if group.faculty != self._faculty:
            raise AccessDeniedError("Can't access to foreign faculty")
        self._groups_access.update({group: access})


class LecturerPermissions(Permissions):

    def __init__(self, lecturer: Lecturer, *args, **kwargs):
        """
        :param lecturer: DB lecturer instance
        """
        self._lecturer = lecturer
        super().__init__(*args, **kwargs)

    @property
    def lecturer(self) -> Lecturer:
        return self._lecturer

    def get_access(self, group: Group, subject: Subject) -> AccessLevel:
        #  breakpoint()
        if subject not in self._lecturer.subjects:
            return AccessLevel.NOACCESS
        return super().get_access(group, subject)


class InstituteManagerPermissions(Permissions):

    def get_access(self, *args, **kwargs) -> AccessLevel:
        return AccessLevel.READ


class User:
    """User of accoutability system."""

    def __init__(self, name, password, email,
                 permissions: Permissions, is_admin=False):
        """
        :param name: User's name
        :param password: User's password
        :param email: User's email
        :param permissions: Declares user's access level to her resources
        :param is_admin: Does user has administrator privileges?
        """
        self._name = name
        self._password_hash = generate_password_hash(password)
        self._email = email
        self._is_admin = is_admin
        self._permissions = permissions

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    @property
    def permissions(self):
        return self._permissions

    @permissions.setter
    def permissions(self, permissions: Permissions):
        if not permissions:
            raise ValueError("Permissions can't be empty")
        self._permissiosn = permissions

    def is_administrator(self):
        return self._is_admin

    def get_access(self, **kwargs):
        return self._permissions.get_access(kwargs)
