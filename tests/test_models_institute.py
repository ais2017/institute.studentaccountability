# -*- coding: utf-8 -*-
import json
import pytest

from app.models.institute import Subject
from . import students_data, groups_data, lecturers_data


@pytest.mark.usefixtures("students_data")
class TestStudent:

    def setup_class(self):
        self.students = students_data()
        assert self.students

    def test_marks_borders(self):
        subj = self.students[0].subjects[0]
        self.students[0].marks = {subj: [4, 5, 4, 1, 3, 2, 2]}
        with pytest.raises(ValueError):
            self.students[0].marks = {subj: [-4, 5, 4, 1, 3, 2, 2]}
        with pytest.raises(ValueError):
            self.students[0].marks = {subj: [5, 4, 6, 3, 2, 2]}

    def test_additional_subjects_borders(self):
        subj = Subject("NewSubject")
        self.students[0].add_additional_subject(subj)

        existing_subj = self.students[0].subjects[0]
        with pytest.raises(ValueError):
            self.students[0].add_additional_subject(existing_subj)

    def test_get_marks(self):
        subj = self.students[0].subjects[0]
        assert self.students[0].get_marks(subj)

        with pytest.raises(ValueError):
            self.students[0].get_marks(Subject("NonExistentSubject"))

    def test_insert_mark(self):
        subj = self.students[0].subjects[0]
        self.students[0].insert_mark(subj, 5)

        with pytest.raises(ValueError):
            self.students[0].insert_mark(subj, 6)
        with pytest.raises(ValueError):
            self.students[0].insert_mark(subj, -3)
        with pytest.raises(ValueError):
            self.students[0].insert_mark(Subject("NonExistentSubject"), 5)


@pytest.mark.usefixtures("groups_data")
class TestGroup:

    def setup_class(self):
        self.groups = groups_data()
        assert self.groups

    def test_subjects_borders(self):
        len_before = len(self.groups[0].subjects)
        self.groups[0].add_subject(Subject("NewSubject"))
        assert len(self.groups[0].subjects) == len_before + 1

        with pytest.raises(ValueError):
            subj = self.groups[0].subjects[0]
            assert self.groups[0].add_subject(subj)
        with pytest.raises(ValueError):
            assert self.groups[0].add_subject(None)

    @pytest.mark.usefixtures("students_data")
    def test_students_borders(self):
        new_student = students_data()[0]

        self.groups[0].add_student(new_student)
        assert len(self.groups[0].students) == 1
        with pytest.raises(ValueError):
            self.groups[0].add_student(new_student)


@pytest.mark.usefixtures("lecturers_data")
class TestLecturers:

    def setup_class(self):
        self.lecturers = lecturers_data()
        assert self.lecturers

    def test_subjects_borders(self):
        len_before = len(self.lecturers[0].subjects)
        self.lecturers[0].add_subject(Subject("NewSubject"))
        assert len(self.lecturers[0].subjects) == len_before + 1

        with pytest.raises(ValueError):
            subj = self.lecturers[0].subjects[0]
            assert self.lecturers[0].add_subject(subj)
        with pytest.raises(ValueError):
            assert self.lecturers[0].add_subject(None)
