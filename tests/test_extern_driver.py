# -*- coding: utf-8 -*-
import pytest
import os

from app.core.extern_driver import ExternSystemJSONConsumer

class TestExternSystemJSONConsumer:
    """
    FIXME: I use hardcoded values that depends on these fixtures. Have no time
    for it now.
    """

    def test_file_locations_exists(self):
        d = ExternSystemJSONConsumer(
                locations=[
                    os.path.abspath("assets/students.json"),
                    os.path.abspath("assets/metadata.json")])
        assert d

        with pytest.raises(OSError):
            ExternSystemJSONConsumer(locations=["not", "exitsts"])

    def test_fetch_students(self):
        d = ExternSystemJSONConsumer(
                locations=[
                    os.path.abspath("assets/students.json"),
                    os.path.abspath("assets/metadata.json")])
        students = d.fetch_students()
        assert len(students) == 6

    def test_fetch_study_data(self):
        d = ExternSystemJSONConsumer(
                locations=[
                    os.path.abspath("assets/students.json"),
                    os.path.abspath("assets/metadata.json")])
        gs, ls = d.fetch_study_data()
        assert len(gs) == 2
        assert len(ls) == 2

    def test_fetch_budget_limit(self):
        d = ExternSystemJSONConsumer(
                locations=[
                    os.path.abspath("assets/students.json"),
                    os.path.abspath("assets/metadata.json")])
        assert d.fetch_budget_limit() == 9000

        d = ExternSystemJSONConsumer(
                locations=[
                    os.path.abspath("assets/students.json"),
                    os.path.abspath("tests/assets/broken_metadata.json")])
        with pytest.raises(ValueError):
            d.fetch_budget_limit()

    def test_fetch_debt_coefficient(self):
        d = ExternSystemJSONConsumer(
                locations=[
                    os.path.abspath("assets/students.json"),
                    os.path.abspath("assets/metadata.json")])
        assert d.fetch_debt_coefficient() == 0.4

        d = ExternSystemJSONConsumer(
                locations=[
                    os.path.abspath("assets/students.json"),
                    os.path.abspath("tests/assets/broken_metadata.json")])
        with pytest.raises(ValueError):
            d.fetch_debt_coefficient()

