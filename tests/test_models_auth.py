# -*- coding: utf-8 -*-
import pytest

from app.models.institute import Subject, Group, Lecturer
from app.models.auth import (
    Permissions,
    FacultyManagerPermissions,
    LecturerPermissions,
    InstituteManagerPermissions,
    AccessDeniedError,
    AccessLevel,
)
from . import permissions_data, lecturers_data, groups_data


@pytest.mark.usefixtures("lecturers_data")
@pytest.mark.usefixtures("permissions_data")
@pytest.mark.parametrize("permissions_data", indirect=True, scope="class",
                         argvalues=[
                             (FacultyManagerPermissions, {
                                 "faculty": "Faculty1"
                             }),
                             (LecturerPermissions, {
                                 "lectuer": lecturers_data()[0]
                             }),
                             (InstituteManagerPermissions, {}),
                         ])
class TestPermissions:
    """Not useful after changes, but I keep it for generalized test cases."""

    pass


@pytest.mark.usefixtures("groups_data")
class TestFacultyManagerPermissions:

    def setup_class(self):
        self.faculty = "TestFaculty"
        self.subject = Subject("TestSubject")
        self.group = groups_data()[0]
        self.permissions = FacultyManagerPermissions(
            name="test_perm",
            faculty=self.faculty,
            groups_access={self.group: AccessLevel.READ},
            subjects_access={self.subject, AccessLevel.WRITE})
        assert self.permissions

    def test_faculty(self):
        assert self.faculty == self.permissions.faculty

    def test_group_borders(self):
        with pytest.raises(ValueError):
            self.permissions.update_group(groups_data()[1], AccessLevel.READ)

    def test_get_access(self):
        assert AccessLevel.NOACCESS == self.permissions.get_access(
                groups_data()[1])


@pytest.mark.usefixtures("lecturers_data", "groups_data")
class TestLecturerPermissions:

    def setup_class(self):
        self.subject = lecturers_data()[0].subjects[0]
        self.lecturer = lecturers_data()[0]
        self.group = groups_data()[0]
        self.permissions = LecturerPermissions(
            name="test_perm",
            lecturer=self.lecturer,
            groups_access={self.group: AccessLevel.READ},
            subjects_access={self.subject, AccessLevel.WRITE})
        assert self.permissions

    def test_lecturer(self):
        assert self.lecturer == self.permissions.lecturer

    def test_get_access(self):
        assert AccessLevel.NOACCESS == self.permissions.get_access(
                groups_data()[1], Subject("New"))


@pytest.mark.usefixtures("groups_data")
class TestInstituteManagerPermissions:

    def setup_class(self):
        self.subject = Subject("TestSubject")
        self.lecturer = lecturers_data()[0]
        self.group = groups_data()[0]
        self.permissions = InstituteManagerPermissions(
            name="test_perm",
            groups_access={self.group: AccessLevel.READ},
            subjects_access={self.subject, AccessLevel.WRITE})
        assert self.permissions

    def test_get_access(self):
        """Can read any data."""
        assert AccessLevel.READ == self.permissions.get_access(
            groups_data()[1])
        assert AccessLevel.READ == self.permissions.get_access(
            groups_data()[0], Subject("New"))
