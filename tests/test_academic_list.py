# -*- coding: utf-8 -*-
import datetime
import json
import pytest

from app.models.institute import Subject, Student
from app.core.academic_list import (
    MaterialIncentivesList,
    AcademicPerformanceList,
    AcademicControlList,
    DeductionList,
    ReportFormatter)

from . import (
    students_data,
    groups_data,
    lecturers_data,
    academic_list_data)


class TestReportFormatter:
    """Tests for ReportFormatter class."""

    fmt = "csv"

    def setup_class(self):
        self.report_formatter = ReportFormatter(self.fmt)

    def test_init_borders(self):
        with pytest.raises(ValueError):
            ReportFormatter("unknown")
        with pytest.raises(ValueError):
            ReportFormatter(None)

    def test_fmt_borders(self):
        with pytest.raises(ValueError):
            self.report_formatter.fmt = "unknown"
        with pytest.raises(ValueError):
            self.report_formatter.fmt = None


@pytest.mark.usefixtures("academic_list_data")
@pytest.mark.parametrize("academic_list_data", indirect=True, scope="class",
                         argvalues=[
                             (MaterialIncentivesList, {
                                 "cash_min": 2000,
                                 "cash_limit": 5000,
                             }),
                             (DeductionList, {}),
                             (AcademicControlList, {
                                 "date_start": datetime.datetime.now(),
                                 "date_end": datetime.datetime.now(),
                             }),
                             (AcademicPerformanceList, {}),
                         ])
class TestAcademicListGeneric:
    """Tests for generic account list methods defined in AcademicList class."""

    def test_init_borders(self, academic_list_data):
        with pytest.raises(ValueError):
            academic_list_data.name = ""

    def test_to_json(self, academic_list_data):
        json_data = json.loads(academic_list_data.to_json())
        assert len(json_data["students"]) == len(academic_list_data)
        assert json_data["name"] == academic_list_data._name


@pytest.mark.usefixtures("students_data")
class TestMaterialIncentivesList:
    """Tests for MaterialIncentivesList methods."""

    name = "TestMaterialList"
    cash_min = 300
    cash_limit = 9000

    def setup_class(self):
        self.alist = MaterialIncentivesList(self.name, students_data(),
                                            self.cash_min, self.cash_limit)
        assert self.alist

    def test_init_borders(self):
        with pytest.raises(ValueError):
            MaterialIncentivesList(self.name, students_data(), 1, -9000)
        with pytest.raises(ValueError):
            MaterialIncentivesList(self.name, students_data(), -300, 1)
        with pytest.raises(ValueError):
            MaterialIncentivesList(self.name, students_data(), -1, -9000)

    def test_cash_limit_borders(self):
        self.alist.cash_limit = 500
        assert self.alist.cash_limit == 500

        with pytest.raises(ValueError):
            self.alist.cash_limit = -1000

    def test__evaluate_addition(self, groups_data):
        students_save = self.alist._students

        best_student = Student("BestStudent", groups_data[0])
        best_student._marks = {Subject(f"Subject{i}"): [
            5] * 10 for i in range(100)}
        self.alist.append(best_student)

        self.alist.sort()
        self.alist._evaluate_addition()

        assert best_student in self.alist
        #  json_data = json.loads(self.alist.to_json())
        #  assert "BestStudent" in json_data["students"]


@pytest.mark.usefixtures("students_data")
class TestAcademicControlList:
    """Tests for AcademicControlList specific methods."""

    name = "TestAcademicControlList"
    date_start = datetime.date(2010, 6, 16)
    date_end = datetime.date(2010, 10, 20)

    def setup_class(self):
        self.alist = AcademicControlList(self.name, students_data(),
                                         self.date_start, self.date_end)
        assert self.alist

    def test_init(self):
        """Test for dates ranges during list initialization."""
        with pytest.raises(ValueError):
            date_start = datetime.date(2017, 6, 16)
            date_end = datetime.date(2011, 10, 20)
            AcademicControlList(self.name, students_data(),
                                date_start, date_end)
        with pytest.raises(ValueError):
            AcademicControlList(self.name, students_data(),
                                None, date_end)
        with pytest.raises(ValueError):
            AcademicControlList(self.name, students_data(),
                                date_start, None)

    def test_date_borders(self):
        """Check various border cases for internal dates period."""
        with pytest.raises(ValueError):
            self.alist.date_start = datetime.date(2017, 6, 16)
        with pytest.raises(ValueError):
            self.alist.date_start = None
        with pytest.raises(ValueError):
            self.alist.date_end = datetime.date(1999, 6, 16)
        with pytest.raises(ValueError):
            self.alist.date_end = None

    def test_get_control_period_weeks(self):
        date_start = datetime.date(2011, 6, 16)
        date_end = datetime.date(2011, 7, 16)

        l = AcademicControlList(self.name, students_data(),
                                date_start, date_end)

        assert l.get_control_period_weeks() == 4

