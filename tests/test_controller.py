# -*- coding: utf-8 -*-
import pytest
import os

from app.core.controller import StudentAccountAbilityController


class TestStudentAccountAbilityController:

    def test_extern_interface(self):
        c = StudentAccountAbilityController("json",
                locations=[
                    os.path.abspath("assets/students.json"),
                    os.path.abspath("assets/metadata.json")])
        assert c.extern_interface == "json"

        with pytest.raises(AttributeError):
            StudentAccountAbilityController("json")

        with pytest.raises(ValueError):
            StudentAccountAbilityController("foo")
