# -*- coding: utf-8 -*-
# flake8: NOQA
from __future__ import absolute_import

import json
import os
import pytest

from typing import List

from app.models.institute import Student, Group, Subject, Lecturer
from app.models.auth import Permissions

ASSETS_PATH = "assets"


@pytest.fixture(scope="class")
def students_data() -> List[Student]:
    students = []
    json_path = os.path.join(os.path.dirname(
        __file__), ASSETS_PATH, "model.json")
    with open(json_path) as f:
        json_data = json.load(f)
        groups = {}
        for gd in json_data.get("groups"):
            g_subjects = [Subject(s) for s in gd["subjects"]]
            groups.update(
                {gd["name"]: Group(gd["name"], gd["faculty"], g_subjects)})
        for sd in json_data.get("students"):
            additional_subjects = [Subject(s)
                                   for s in sd["additional_subjects"]]
            students.append(
                Student(sd["name"], groups[sd["group"]], additional_subjects))
    return students


@pytest.fixture(scope="class")
def groups_data() -> List[Group]:
    groups = []
    json_path = os.path.join(os.path.dirname(
        __file__), ASSETS_PATH, "model.json")
    with open(json_path) as f:
        json_data = json.load(f)
        for gd in json_data.get("groups"):
            # mypy: begin ignore
            g_subjects = [Subject(s) for s in gd["subjects"]]
            groups.append(Group(gd["name"], gd["faculty"], g_subjects))
            # mypy: end ignore
    return groups


@pytest.fixture(scope="class")
def lecturers_data() -> List[Lecturer]:
    lecturers = []
    json_path = os.path.join(os.path.dirname(
        __file__), ASSETS_PATH, "model.json")
    with open(json_path) as f:
        json_data = json.load(f)
        for ld in json_data.get("lecturers"):
            # mypy: begin ignore
            l_subjects = [Subject(s) for s in ld["subjects"]]
            lecturers.append(Lecturer(ld["name"], l_subjects))
            # mypy: end ignore
    return lecturers


@pytest.fixture(scope="class")
def permissions_data(request):
    args = {"name": "TestPermissions"}
    args.update(request.param[1])
    cls = request.param[0].__new__(request.param[0])
    cls.__init__(**args)
    return cls


@pytest.fixture(scope="class")
def academic_list_data(request, students_data):
    """
    :param request: AccountList subclass.
    """
    args = {
        "name": "TestAccountList",
        "students": students_data,
    }
    args.update(request.param[1])
    cls = request.param[0].__new__(request.param[0])
    cls.__init__(**args)
    return cls
